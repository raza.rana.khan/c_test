

#include <iostream>
#include <fstream>
#include "C_Test.h"
using namespace std;



struct Node {
	string data;
	int freq;
	struct Node *next;
};

//link list
class list
{
	private:
		Node *head;
		
	public:
		
		list()
		{
			head = NULL;
		}

		//insert on priority basis of frequency
		void insert(Node* node) {
			if (head==NULL) {
				head = node;
			}

			else if (head->freq < node->freq) {
				node->next = head;
				head = node;
			}
			else {
				Node* curr = new Node;
				Node* previous = new Node;


				previous = head;
				curr = head->next;
				while (curr != NULL) {
					if (curr->freq <= node->freq) {//searching the node with lower frequency than the one we want to add
						previous->next = node;
						node->next = curr;
						return;
					}
					previous = curr;
					curr = curr->next;
				}
				previous->next = node;

			}
			
		}
		//insert new if subset doesnot exists
		void insert_new(string str) {
			Node *new_node = new Node;

			new_node->data = str;
			new_node->freq = 1;
			new_node->next = NULL;
			insert(new_node);
		}

		//search subset already exists
		void search_and_insert(string str) {
			
			if (head==NULL) {
				insert_new(str);
			}
			
			else {
				Node *temp = new Node;
				Node *previous = new Node;
				temp = head;
				previous = temp;
				while (temp != NULL) {
					if (temp->data == str) {
						temp->freq++;
						previous->next = temp->next; //deleting from current location 
						insert(temp);//insert in the list again on basis of freq
						return;
					}
					previous = temp;
					temp = temp->next;
				}
				insert_new(str);

			}
			
			
		}
		//display all nodes
		void display_all() {
			struct Node *temp=new Node;
			temp = head;
			while (temp != NULL) {
				cout << temp->data << " " << temp->freq << endl;
				temp = temp->next;
			}

		}
		//calculte the percentage of 10 most common words combinations
		void calculate_Percentage() {
			struct Node *temp = new Node;
			temp = head;
			double total = 0;
			for (int i = 0; i < 10 && (temp!=NULL);i++) {
				total += temp->freq;
				temp = temp->next;
			}
			
			temp = head;
			for (int i = 0; i < 10 && temp != NULL; i++) {
				double p = ((double(temp->freq) / total * 100.00) );
				cout.precision(2);
				cout << temp->data<<": "<<fixed<<p<<"%\t";
				for (int j = 0; j < p;j++) {
					cout << " * ";
				}
				cout << endl;
				cout << endl;
				temp = temp->next;
			}

		}

		
};











void looper(int subset,char chr, ifstream& test_file,string word,list& list1) {
	//starting from 4 set size with keep increasing until we find either end of line, new line or space
	for (int i = 0; i <= subset; i++) {
		chr = test_file.get();
		if (chr != EOF && chr != ' ' && chr != '\n')
		{


			word = word + chr;

		}

		else {
			//check if previous word have enough chars to produce more sets with length greater than 3
			if ((int(word.size()) - 4) > 0) {

				if (chr == EOF)subset -= 1;
				if (chr == '\n')subset += 1;
				test_file.clear();
				test_file.seekg(-subset, ios_base::cur);//if yes we will read the word again from specific location

			}
			else if (chr == EOF) {
				return;
			}



			word.clear();
			subset = 3;
			return looper(subset,chr,test_file,word, list1);//recursively calling the function again
		}


	}

	//we will reach here when we succesfully find a word with length more than 3 letters
	subset++;

	test_file.clear();
	test_file.seekg(-subset, ios_base::cur);
	list1.search_and_insert(word);
	
	word.clear();
	return looper(subset, chr, test_file, word, list1);
}

void read_file(string filename, list& list1) {
	ifstream test_file;
	test_file.open(filename);

	if (!test_file) {
		cout << "File Not found";
		exit(1);
	}
	string word;
	char chr = ' ';
	word.clear();
	int subset = 3;
	looper(subset, chr, test_file, word, list1);//recursive function to find all the sets
	//i also implemented the solution with loop but only adding this 
}

int main()
{

	list list1;
	read_file("test_cases.txt", list1);

	list1.calculate_Percentage();
	return 0;
}

